package com.mballen.curso.boot.camelTest;

import com.mballen.curso.boot.domain.Cargo;
import com.mballen.curso.boot.domain.Departamento;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.stereotype.Component;

@Component
public class MyRouteBuilder extends RouteBuilder {

    public void configure() {

        //inserindo departamentos
        from("file:src/data/departamentos?noop=true")
                .unmarshal().json(JsonLibrary.Jackson, Departamento.class)
                .to("sql:insert into DEPARTAMENTOS (nome) " + "VALUES(:#${body.nome})");

        //inserindo cargos
        from("file:src/data/cargos?noop=true")
                .delay(500).asyncDelayed()
                .unmarshal().json(JsonLibrary.Jackson, Cargo.class)
                .to("sql:insert into CARGOS (nome,id_departamento_fk) " +
                        "VALUES(:#${body.nome},:#${body.id})");
    }
}